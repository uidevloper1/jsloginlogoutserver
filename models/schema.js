//INPUT MODULES
const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const userSchema=new Schema({
    first_Name:{
        type:String,
        required:true
    },
    last_Name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    }
})

module.exports=mongoose.model('users',userSchema);