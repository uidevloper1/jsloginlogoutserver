//create the express module
const express = require('express');
const router = express.Router();//create rest api router module
const mongoose=require('mongoose');//create the mongoose module
const cors=require('cors');//create the cors module for use port
const jwt=require('jsonwebtoken');//use jwt module for json web token
const bcrypt=require('bcryptjs');//use bcrypt module for decript and encript the password
const User=require('../models/schema');//use the schema modole
router.use(cors());//enable the port
process.env.SECRET_KEY = 'secret';
//connect the mongo database
mongoose.connect("mongodb://localhost:27017/LoginLogOut",(err)=>{
  if(err){
    console.log('DB Connection fail..');
  }else{
    console.log('DB Connection success...');
  }
});

//create post rest api for registation 
router.post("/register",(req,res)=>{
  //To create the today date
  const today=new Date();
  const userData={
    first_Name:req.body.first_Name,
    last_Name:req.body.last_Name,
    email:req.body.email,
    password:req.body.password,
    created:today
  };
  //find the email from database already data is there or not
    User.findOne({email:req.body.email},(err,oldUser)=>{
      if(err){
        throw err;
      }else{
        //check alredy user is there or not
        if(!oldUser){
          // given password is insert in decription format
          //hash is count the length of given password
          bcrypt.hash(req.body.password,10,(err,hash)=>{
            userData.password=hash;
            //create the new user data
            User.create(userData,(err,newUser)=>{
              if(newUser){
                res.status(200).json({status:newUser.email+' '+'is registed successfully'});
              }else{
                res.send('something went wrong',+err);
              }
            });
          });
        }else{
          res.status(422).json({status:oldUser.email+' '+'already registed'})
        }
      }
    });
});

 


/*
router.post("/register",(req,res,next)=>{
  const userData={
    first_Name:req.body.first_Name,
    last_Name:req.body.last_Name,
    email:req.body.email,
    password:req.body.password
  };
    User.findOne({email:req.body.email},(err,result)=>{
      if(!result){
        User.create(userData,(err,result)=>{
          if(result){
            res.json({status:result.email+''+'already registed'});
          }else{
            res.send('error:'+err);
          }
        });
      }else{
        res.send('user alread exist')
      }
      res.send('error:'+err)
    });
});

*/
/* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });

//login module
router.post("/login",(req,res)=>{
  var email=req.body.email;
  var password=req.body.password
	  User.findOne({"email":email},(err,user)=>{
	  if(err){
	    res.send('connection error')
	      }else{
	        if(user){
	          if(bcrypt.compareSync(password,user.password)){
		          var payload={
                _id:user._id,
		            first_Name:user.first_Name,
		            last_Name:user.last_Name,
                email:user.email
		          };
		          let token=jwt.sign(payload,'secret',{expiresIn:3000});
              return res.status(200).json({token:token});
              //return res.status(501).json({message:"login success..."})
              
		         }else{
              //res.status(200).json({token:token});
              return res.status(501).json({Error:"insert correct password"})
             }
	        }else{
	        res.json({error:"user dose not Exist"});
	        }
        }
	  });
});
module.exports = router;
